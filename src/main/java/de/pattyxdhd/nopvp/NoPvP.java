package de.pattyxdhd.nopvp;

import de.pattyxdhd.nopvp.commands.TogglePvPCommand;
import de.pattyxdhd.nopvp.data.Data;
import de.pattyxdhd.nopvp.file.FileManager;
import de.pattyxdhd.nopvp.listener.EntityDamageListener;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class NoPvP extends JavaPlugin {

    @Getter
    private static NoPvP instance;

    @Getter
    private FileManager fileManager;

    @Override
    public void onEnable() {
        instance = this;
        fileManager = new FileManager(getInstance());

        loadCommands();
        loadListener(Bukkit.getPluginManager());


        log("§aPlugin geladen.");
        log("§bby PattyXDHD");
    }

    @Override
    public void onDisable() {
        log("§cPlugin entladen.");
    }

    private void loadCommands(){
        getCommand("togglepvp").setExecutor(new TogglePvPCommand());
    }

    private void loadListener(final PluginManager pluginManager){
        pluginManager.registerEvents(new EntityDamageListener(), this);
    }

    public void log(final String message){
        Bukkit.getConsoleSender().sendMessage(Data.getPrefix() + message);
    }


}
