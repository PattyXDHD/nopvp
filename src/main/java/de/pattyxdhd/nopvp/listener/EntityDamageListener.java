package de.pattyxdhd.nopvp.listener;

import de.pattyxdhd.nopvp.NoPvP;
import de.pattyxdhd.nopvp.data.Data;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

public class EntityDamageListener implements Listener {

    @EventHandler
    public void onDamage(final EntityDamageByEntityEvent event){
        if(event.getEntity() instanceof Player && event.getDamager() instanceof Player){
            final Player killer = ((Player) event.getDamager());

            if(!NoPvP.getInstance().getFileManager().getPvP()){
                if(killer.hasPermission("pvp.admin")){
                    return;
                }
                event.setCancelled(true);
                killer.sendMessage(Data.getPrefix() + "§cPvP ist zur Zeit nicht aktiviert.");
            }
        }
    }

}
