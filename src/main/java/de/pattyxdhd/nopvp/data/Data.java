package de.pattyxdhd.nopvp.data;

import lombok.Getter;

public class Data {

    @Getter
    private final static String prefix = "§8[§cNoPvP§8] §7";

    @Getter
    private final static String noPerm = getPrefix() + "§cDazu hast du keinen Zugriff.";

}
