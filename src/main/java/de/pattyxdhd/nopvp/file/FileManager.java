package de.pattyxdhd.nopvp.file;

import de.pattyxdhd.nopvp.NoPvP;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;

@Getter @Setter(AccessLevel.PRIVATE)
public class FileManager {

    private FileWriter fileWriter;

    public FileManager(final NoPvP noPvP){
        fileWriter = new FileWriter(noPvP.getDataFolder().getPath(), "config.yml");
        load();
    }

    private void load(){
        fileWriter.setDefaultValue("PvP", true);
    }

    public Boolean getPvP(){
        return fileWriter.getBoolean("PvP");
    }

    public void setPvP(final boolean pvp){
        fileWriter.setValue("PvP", pvp);
        fileWriter.save();
    }

}
