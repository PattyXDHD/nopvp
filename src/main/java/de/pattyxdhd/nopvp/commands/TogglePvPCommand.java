package de.pattyxdhd.nopvp.commands;

import de.pattyxdhd.nopvp.NoPvP;
import de.pattyxdhd.nopvp.data.Data;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class TogglePvPCommand implements CommandExecutor {


    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if(sender.hasPermission("pvp.toggle")){
            if(args.length == 0){
                if(NoPvP.getInstance().getFileManager().getPvP()){
                    NoPvP.getInstance().getFileManager().setPvP(false);
                    sender.sendMessage(Data.getPrefix() + "§cDu hast PvP deaktiviert.");
                }else{
                    NoPvP.getInstance().getFileManager().setPvP(true);
                    sender.sendMessage(Data.getPrefix() + "§aDu hast PvP aktiviert.");
                }
            }else{
                sender.sendMessage(Data.getPrefix() + "Usage: /togglepvp");
            }
        }else{
            sender.sendMessage(Data.getNoPerm());
        }

        return false;
    }


}
